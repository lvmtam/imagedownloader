//
//  NSString+Random.h
//  ImageDownloader
//
//  Created by lvmtam on 11/26/16.
//  Copyright © 2016 lvmtam. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (Random)

+ (NSString *)generateAUniqueText;

@end
