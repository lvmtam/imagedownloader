//
//  NSString+Random.m
//  ImageDownloader
//
//  Created by lvmtam on 11/26/16.
//  Copyright © 2016 lvmtam. All rights reserved.
//

#import "NSString+Random.h"



@implementation NSString (Random)

+ (NSString *)generateAUniqueText {
    return [NSUUID UUID].UUIDString;
    return [NSString stringWithFormat:@"%f", [[NSDate date] timeIntervalSince1970]];
}

@end
