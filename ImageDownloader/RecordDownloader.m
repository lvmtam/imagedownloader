//
//  RecordDownloader.m
//  ImageDownloader
//
//  Created by Tam Le on 11/25/16.
//  Copyright © 2016 lvmtam. All rights reserved.
//

#import "RecordDownloader.h"
#import "DataDownloader.h"
#import "ZipFileManager.h"



@interface RecordDownloader ()
@property (nonatomic, strong) DataDownloader *dataDownloader;
@end


@implementation RecordDownloader

- (instancetype)initWithRecord:(Record *)record {
    self = [super init];
    if (self) {
        self.record = record;
        self.progress = [NSProgress progressWithTotalUnitCount:10];
        self.progress.cancellable = YES;
        self.progress.pausable = YES;
        self.dataDownloader = [[DataDownloader alloc] initWithRecord:self.record];
    }
    return self;
}



- (void)main {
    [super main];
    
    if (self.isCancelled) {
        [self finish];
        return;
    }

    [self.progress addChild:self.dataDownloader.progress withPendingUnitCount:7];
    
    // download data
    __weak RecordDownloader *weakSelf = self;
    self.dataDownloader.completionBlock = ^ {
        
        // download failure
        if (weakSelf.dataDownloader.downloadError) {
            weakSelf.record.state = RecordStateError;
            [weakSelf finish];
            return;
        }
        
        if (weakSelf.isCancelled) {
            NSLog(@"operation %@ be cancelled.", weakSelf);
            [weakSelf finish];
            return;
        }
        
        // unzip if need
        [weakSelf.progress becomeCurrentWithPendingUnitCount:3];
        if (weakSelf.record.type == RecordTypeZip) {
            weakSelf.record.state = RecordStateUnzipping;
            
            [ZipFileManager unzipFile:weakSelf.record.storedDataUrl.absoluteString completion:^(NSArray<NSString *> *paths, NSError *error) {
                
                if (weakSelf.isCancelled) {
                    NSLog(@"operation %@ be cancelled.", weakSelf);
                    [weakSelf finish];
                    return;
                }
                
                if (error) {
                    weakSelf.record.state = RecordStateError;
                    [weakSelf finish];
                    return;
                }
                weakSelf.record.storedDataUrl = [NSURL URLWithString:paths.firstObject];
                // update record state
                weakSelf.record.state = RecordStateFinished;
                // finish operation
                [weakSelf finish];
            }];
        }
        [weakSelf.progress resignCurrent];
        
        // update record state
        weakSelf.record.state = RecordStateFinished;
        // finish operation
        [weakSelf finish];
    };
    [self.dataDownloader start];
    
    // update record state
    self.record.state = RecordStateDownloading;
}

- (void)finish {
    // because we want to NSProgress completion eventhough a error occurred. So set it by manually.
    self.progress.completedUnitCount = self.progress.totalUnitCount;
    [super finish];
}

@end
