//
//  PendingOperations.m
//  ImageDownloader
//
//  Created by lvmtam on 11/24/16.
//  Copyright © 2016 lvmtam. All rights reserved.
//

#import "AsynchronizeOperation.h"


@interface AsynchronizeOperation ()
@property (nonatomic, readwrite) BOOL mFinished;
@property (nonatomic, readwrite) BOOL mExecuting;
@property (nonatomic, readwrite) BOOL finishInMain;
@end


@implementation AsynchronizeOperation


- (void)start {
    if (self.cancelled) {
        [self finish];
        return;
    }
    
    [self willChangeValueForKey:@"isExecuting"];
    self.mExecuting = YES;
    [self didChangeValueForKey:@"isExecuting"];
    
    // Call main, maybe other subclasses will want use it?
    // We have to call it manually when overriding `start`.
    [self main];
}

- (void)main {
    if (self.cancelled && self.mFinished != YES) {
        [self finish];
        return;
    }
    
    if (self.finishInMain) {
        [self finish];
    }
}


- (void)finish {
    [self willChangeValueForKey:@"isFinished"];
    [self willChangeValueForKey:@"isExecuting"];
    self.mExecuting = NO;
    self.mFinished = YES;
    [self didChangeValueForKey:@"isExecuting"];
    [self didChangeValueForKey:@"isFinished"];
}


- (BOOL)isFinished {
    return self.mFinished;
}

- (BOOL)isExecuting {
    return self.mExecuting;
}

@end
