//
//  ViewController.m
//  ImageDownloader
//
//  Created by lvmtam on 11/24/16.
//  Copyright © 2016 lvmtam. All rights reserved.
//

#import "AlbumsViewController.h"
#import "AlbumCell.h"
#import "AlbumManager.h"
#import "RecordsViewController.h"
#import "PendingOperations.h"


static void *AlbumsViewControllerObservationContext = &AlbumsViewControllerObservationContext;


@interface AlbumsViewController() <UITableViewDataSource, UITableViewDelegate>

@property (weak, nonatomic) IBOutlet UITableView *albumsTableView;
@property (weak, nonatomic) IBOutlet UISlider *queueSlider;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *resetBarButtomItem;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *addBarButtonItem;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *pauseResumeButtonItem;

@property (nonatomic, strong) NSMutableArray *albums;
@property (nonatomic) BOOL paused;

@end



@implementation AlbumsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.albums = [NSMutableArray array];
    self.albumsTableView.estimatedRowHeight = 60;
    self.albumsTableView.rowHeight = UITableViewAutomaticDimension;
    
    [self updateMaximumConcurrentCount];
    [self updateNavigationButtons];
}

- (void)updateNavigationButtons {
    self.pauseResumeButtonItem.title = self.paused ? @"Resume" : @"Pause";
    self.pauseResumeButtonItem.enabled = self.albums.count > 0 ? YES : NO;
}

- (void)updateMaximumConcurrentCount {
    [PendingOperations sharedInstance].maxConcurrentCount = (NSUInteger)self.queueSlider.value;
}

- (void)addAlbums {
    // get albums from network
    [AlbumManager fetchAlbumsWithCompletion:^(NSArray<Album *> *albums, NSError *error) {
        
        // toast if have error
        if (error) {
            UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Error" message:@"Add new albums failure!" preferredStyle:UIAlertControllerStyleAlert];
            UIAlertAction *close = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
            [alert addAction:close];
            [self presentViewController:alert animated:YES completion:nil];
            return;
        }
        
//        albums = @[albums[5], albums[6]];
        
        // reload tableview
        [self.albums addObjectsFromArray:albums];
        [self.albumsTableView reloadData];
        
        [self updateNavigationButtons];
        
        // kick-off start operation
        [self startDownloadResourcesForAlbums:albums];
    }];
}

- (void)startDownloadResourcesForAlbums:(NSArray *)albums {
    for (Album *album in albums) {
        [album startDownloadResources];
    }
}

- (void)resetDownloadResources {
    // cancel all operations
    [[PendingOperations sharedInstance] reset];
    // cancel NSProgress
    for (Album *album in self.albums) {
        [album reset];
    }
    // clear all albums
    self.albums = [NSMutableArray array];
    self.paused = NO;
}

- (void)pauseDownloadResources {
    if (self.paused) { return; }
    // pause all operations
    [[PendingOperations sharedInstance] pause];
    // pause NSProgress
    for (Album *album in self.albums) {
        [album pause];
    }
    self.paused = YES;
}

- (void)resumeDownloadResource {
    if (!self.paused) { return; }
    // resume all operations
    [[PendingOperations sharedInstance] resume];
    // resume NSProgress
    for (Album *album in self.albums) {
        [album resume];
    }
    self.paused = NO;
}



#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.albums.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    AlbumCell *cell = [tableView dequeueReusableCellWithIdentifier:@"AlbumCell" forIndexPath:indexPath];
    Album *album = self.albums[indexPath.row];
    cell.album = album;
    return cell;
}



#pragma mark - UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:NO];
    [self performSegueWithIdentifier:@"ShowAlbumSegue" sender:[tableView cellForRowAtIndexPath:indexPath]];
}

// For remove redundant cell separate line
- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    return 0.01f;
}
- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section {
    return [UIView new];
}



#pragma mark - Segue

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:@"ShowAlbumSegue"]) {
        NSIndexPath *index = [self.albumsTableView indexPathForCell:sender];
        RecordsViewController *controller = segue.destinationViewController;
        controller.album = self.albums[index.row];
    }
}



#pragma mark - UI Events

- (IBAction)addAlbumsButtonDidPressed:(id)sender {
    [self addAlbums];
}

- (IBAction)resetAlbumsButtonDidPressed:(id)sender {
    [self resetDownloadResources];
    [self.albumsTableView reloadData];
    [self updateNavigationButtons];
}

- (IBAction)pauseAlbumsButtonDidPressed:(id)sender {
    if (self.paused) {
        [self resumeDownloadResource];
    } else {
        [self pauseDownloadResources];
    }
    [self updateNavigationButtons];
}

- (IBAction)sliderValueDidChanged:(UISlider *)sender {
    sender.value = (int)sender.value;
    [self updateMaximumConcurrentCount];
}


@end
