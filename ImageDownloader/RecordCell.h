//
//  RecordCell.h
//  ImageDownloader
//
//  Created by lvmtam on 11/24/16.
//  Copyright © 2016 lvmtam. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Record.h"


@interface RecordCell : UICollectionViewCell
@property (nonatomic, strong) Record *record;
@end
