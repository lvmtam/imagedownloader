//
//  AlbumCell.m
//  ImageDownloader
//
//  Created by lvmtam on 11/24/16.
//  Copyright © 2016 lvmtam. All rights reserved.
//

#import "AlbumCell.h"
#import "Album+Displayable.h"


static NSString *fractionCompletedKeyPath = @"progress.fractionCompleted";
static void *AlbumCellObserverContext = &AlbumCellObserverContext;


@interface AlbumCell()
@property (weak, nonatomic) IBOutlet UILabel *title;
@property (weak, nonatomic) IBOutlet UILabel *subtitle;
@property (weak, nonatomic) IBOutlet UIProgressView *progressView;
@end



@implementation AlbumCell

- (void)setAlbum:(Album *)album {
    // remove old observers
    if (_album) {
        [_album removeObserver:self forKeyPath:fractionCompletedKeyPath context:AlbumCellObserverContext];
    }
    // set new album
    _album = album;
    // add new observers
    if (_album) {
        [_album addObserver:self forKeyPath:fractionCompletedKeyPath options:0 context:AlbumCellObserverContext];
    }
    
    [self updateGUI];
}

- (void)updateGUI {
    self.title.text = self.album.name;
    self.subtitle.text = self.album.progressText;
    
    BOOL shouldHide = self.album.progressState == AlbumProcessStateQueuing;
    self.progressView.progress = self.album.progress.fractionCompleted;
    if (self.progressView.hidden != shouldHide) {
        self.progressView.hidden = shouldHide;
    }
}

- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary<NSKeyValueChangeKey,id> *)change context:(void *)context {
    if (context == AlbumCellObserverContext && [keyPath isEqualToString:fractionCompletedKeyPath]) {
        dispatch_async(dispatch_get_main_queue(), ^{
            [self updateGUI];
        });
    } else {
        [super observeValueForKeyPath:keyPath ofObject:object change:change context:context];
    }
}

- (void)dealloc {
    if (self.album) {
        [self removeObserver:self forKeyPath:fractionCompletedKeyPath context:AlbumCellObserverContext];
    }
}

@end
