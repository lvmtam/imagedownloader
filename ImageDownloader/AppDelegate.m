//
//  AppDelegate.m
//  ImageDownloader
//
//  Created by lvmtam on 11/24/16.
//  Copyright © 2016 lvmtam. All rights reserved.
//

#import "AppDelegate.h"
#import "UIImage+Extension.h"


@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    [self clearTemporaryDicrectory];
    return YES;
}

- (void)applicationWillTerminate:(UIApplication *)app {
    [self clearTemporaryDicrectory];
}

- (void)clearTemporaryDicrectory {
    // clear all files in temp folder
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSError *error;
    NSArray *tempFiles = [fileManager contentsOfDirectoryAtPath:NSTemporaryDirectory() error:&error];
    for (NSString *file in tempFiles) {
        error = nil;
        [fileManager removeItemAtPath:[NSTemporaryDirectory() stringByAppendingPathComponent:file] error:&error];
    }
}

- (void)applicationDidReceiveMemoryWarning:(UIApplication *)application {
    // when receive memory warning, we will release all cached images.
    [UIImage clearCache];
}



#pragma mark - Background

- (void)application:(UIApplication *)application handleEventsForBackgroundURLSession:(NSString *)identifier
  completionHandler:(void (^)())completionHandler {
    NSLog(@"Session:%@ be called handleEvents.", identifier);
    self.backgroundSessionCompletionHandler = completionHandler;
}



#pragma mark - Networks

- (void)setNetworkActivityIndicatorVisible:(BOOL)visible {
    static NSInteger executingCount;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        executingCount = 0;
    });
    if (visible) {
        executingCount++;
    } else {
        executingCount--;
    }
    [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:(executingCount > 0)];
}

- (void)setLoadingIndicatorVisible:(BOOL)visible {
    
}


@end
