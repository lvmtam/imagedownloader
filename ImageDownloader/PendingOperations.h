//
//  PendingOperations.h
//  ImageDownloader
//
//  Created by lvmtam on 11/24/16.
//  Copyright © 2016 lvmtam. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Album.h"


@interface PendingOperations : NSObject
@property (nonatomic, strong) NSOperationQueue *downloadQueue;
@property (nonatomic) NSUInteger maxConcurrentCount;

+ (instancetype)sharedInstance;

- (void)reset;
- (void)resetOperationsBelongAlbum:(Album *)album;
- (void)pause;
- (void)resume;

@end
