//
//  DataDownloader.m
//  ImageDownloader
//
//  Created by lvmtam on 11/24/16.
//  Copyright © 2016 lvmtam. All rights reserved.
//

#import "DataDownloader.h"
#import "AppDelegate.h"
#import "NSString+Random.h"



@interface DataDownloader() <NSURLSessionDownloadDelegate>
@property (nonatomic, strong) NSURLSessionDownloadTask *task;
@end



@implementation DataDownloader

- (instancetype)initWithRecord:(Record *)record {
    self = [super init];
    if (self) {
        self.record = record;
        self.progress = [NSProgress progressWithTotalUnitCount:-1];
        self.progress.kind = NSProgressKindFile;
        self.progress.cancellable = YES;
        self.progress.pausable = YES;
        [self progressHandlerConfigure];
    }
    return self;
}


- (void)main {
    [super main];
    
    if (self.isCancelled) {
        [self finish];
        return;
    }
    
    [self showNetworkActivityIndicator];
    NSURLSessionConfiguration *configure = [NSURLSessionConfiguration backgroundSessionConfigurationWithIdentifier:[NSUUID UUID].UUIDString];
    NSURLSession *session = [NSURLSession sessionWithConfiguration:configure delegate:self delegateQueue:[NSOperationQueue mainQueue]];
    self.task = [session downloadTaskWithURL:self.record.url];
    [self.task resume];
    [session finishTasksAndInvalidate];
}


- (void)progressHandlerConfigure {
    __weak DataDownloader *weakSelf = self;
    self.progress.cancellationHandler = ^ {
        NSLog(@"progress had cancel the %@ operation.", weakSelf);
        [weakSelf.task cancel];
        [weakSelf finish];
    };
    self.progress.pausingHandler = ^ {
        NSLog(@"progress had pause the %@ operation.", weakSelf);
        [weakSelf.task suspend];
    };
    self.progress.resumingHandler = ^ {
        NSLog(@"progress had resume the %@ operation.", weakSelf);
        [weakSelf.task resume];
    };
}



#pragma mark - NSURLSessionDownloadDelegate

- (void)URLSession:(NSURLSession *)session downloadTask:(NSURLSessionDownloadTask *)downloadTask didWriteData:(int64_t)bytesWritten totalBytesWritten:(int64_t)totalBytesWritten totalBytesExpectedToWrite:(int64_t)totalBytesExpectedToWrite {
    if (self.progress.totalUnitCount == -1) {
        self.progress.totalUnitCount = totalBytesExpectedToWrite;
    }
    self.progress.completedUnitCount = totalBytesWritten;
}

- (void)URLSession:(NSURLSession *)session downloadTask:(NSURLSessionDownloadTask *)downloadTask didResumeAtOffset:(int64_t)fileOffset expectedTotalBytes:(int64_t)expectedTotalBytes {
    self.progress.totalUnitCount = expectedTotalBytes;
    self.progress.completedUnitCount = fileOffset;
}

- (void)URLSession:(NSURLSession *)session downloadTask:(NSURLSessionDownloadTask *)downloadTask didFinishDownloadingToURL:(NSURL *)location {
    NSLog(@"record:%@ [OK]", self.record.url.absoluteString);
    [self downloadDidCompletionWithLocation:location error:nil];
}

- (void)URLSession:(NSURLSession *)session task:(NSURLSessionTask *)task didCompleteWithError:(nullable NSError *)error {
    if (error) {
        NSLog(@"record:%@ [NG]", self.record.url.absoluteString);
        [self downloadDidCompletionWithLocation:nil error:error];
    }
}



#pragma mark - Private methods

- (void)downloadDidCompletionWithLocation:(NSURL *)location error:(NSError *)error {
    // hide network indicator at device's top navigation bar
    [self hideNetworkActivityIndicator];
    // store error
    if (self.isCancelled) {
        self.downloadError = [NSError errorWithDomain:NSCocoaErrorDomain code:NSUserCancelledError userInfo:nil];
    }
    if (error) {
        self.downloadError = error;
    } else {
        if (location) {
            self.record.storedDataUrl = [NSURL URLWithString:[self writeDownloadFileIntoTemporaryDirectory:location error:&error]];
            self.downloadError = error;
        }
    }
    // completion
    [self finish];
}

- (NSString *)writeDownloadFileIntoTemporaryDirectory:(NSURL *)source error:(NSError *__autoreleasing*)error {
    NSString *fileName = [NSString generateAUniqueText];
    NSString *filePath = NSTemporaryDirectory();
    filePath = [filePath stringByAppendingPathComponent:fileName];
    @try {
        NSData *data = [NSData dataWithContentsOfURL:source];
        if ([data writeToFile:filePath atomically:YES]) {
            return filePath;
        } else {
            if (error) {
                *error = [NSError errorWithDomain:@"imagedownloader.DataDownloader" code:2 userInfo:@{@"reason": @"fatal: have error when store file into tmp folder."}];
            }
            return nil;
        }
    }
    @catch (NSException *e) {
        NSLog(@"fatal: write file into tmp folder failure. error %@", e);
        if (error) {
            *error = [NSError errorWithDomain:@"imagedownloader.DataDownloader" code:1 userInfo:e.userInfo];
        }
        return nil;
    }
}

- (void)showNetworkActivityIndicator {
    [(AppDelegate *)[[UIApplication sharedApplication] delegate] setNetworkActivityIndicatorVisible:YES];
}

- (void)hideNetworkActivityIndicator {
    [(AppDelegate *)[[UIApplication sharedApplication] delegate] setNetworkActivityIndicatorVisible:NO];
}



#pragma mark - Overrides

- (void)finish {
    // because we want to NSProgress completion eventhough a error occurred. So set it by manually.
    self.progress.completedUnitCount = self.progress.totalUnitCount;
    [super finish];
}


@end
