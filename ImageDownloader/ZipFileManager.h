//
//  ZipFileManager.h
//  ImageDownloader
//
//  Created by lvmtam on 11/24/16.
//  Copyright © 2016 lvmtam. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ZipFileManager : NSObject

+ (void)downloadAndUnzip:(NSString *)urlPath completion:(void(^)(BOOL succeeded, NSString *unzipPath))completion;
+ (void)downloadAndUnzip:(NSString *)urlPath folderName:(NSString *)folderName completion:(void(^)(BOOL succeeded, NSString *unzipPath))completion;

+ (void)unzipFile:(NSString *)urlPath completion:(void(^)(NSArray<NSString *> *paths, NSError *error))completion;

@end
