//
//  AlbumManager.h
//  ImageDownloader
//
//  Created by lvmtam on 11/24/16.
//  Copyright © 2016 lvmtam. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Album.h"
#import "ZipFileManager.h"



@interface AlbumManager : NSObject

@property (nonatomic, strong) NSOperationQueue *downloadQueue;
@property (nonatomic, strong) NSMutableDictionary *downloadsInProgress;

+ (void)fetchAlbumsWithCompletion:(void (^)(NSArray<Album *> *albums, NSError *error))completion;

+ (instancetype)sharedInstance;

@end
