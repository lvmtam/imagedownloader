//
//  ZipFileManager.m
//  ImageDownloader
//
//  Created by lvmtam on 11/24/16.
//  Copyright © 2016 lvmtam. All rights reserved.
//

#import <SSZipArchive/SSZipArchive.h>
#import "ZipFileManager.h"
#import "NSString+Random.h"



@implementation ZipFileManager


+ (void)downloadAndUnzip:(NSString *)urlPath completion:(void(^)(BOOL succeeded, NSString *unzipPath))completion {
    NSString *folderName = [NSString generateAUniqueText];
    return [ZipFileManager downloadAndUnzip:urlPath folderName:folderName completion:completion];
}


+ (void)downloadAndUnzip:(NSString *)urlPath folderName:(NSString *)folderName completion:(void(^)(BOOL succeeded, NSString *unzipPath))completion {
    dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
    dispatch_async(queue, ^{

        // path info
        NSURL *url = [NSURL fileURLWithPath:[[NSBundle mainBundle] pathForResource:@"JSON files updated" ofType:@"zip"]]; // URLWithString:urlPath];
        NSData *data = [NSData dataWithContentsOfURL:url];
        NSString *fileName = url.path.lastPathComponent;
        NSString *filePath = [NSTemporaryDirectory() stringByAppendingPathComponent:fileName];
        [data writeToFile:filePath atomically:YES];
        
        // write info
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString *documentsDicrectory = [paths firstObject];
        NSString *destinationPath = [documentsDicrectory stringByAppendingPathComponent:folderName];
        
        // unzip
        [SSZipArchive unzipFileAtPath:filePath
                        toDestination:destinationPath
                            overwrite:YES
                             password:nil
                      progressHandler:nil
                    completionHandler:^(NSString *path, BOOL succeeded, NSError *error) {
            
            // for debug only
            if (succeeded) {
                NSLog(@"unzip succeeded!");
            } else {
                NSLog(@"unzip failure, error %@", error);
            }
            
            if (completion) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    completion(succeeded, [destinationPath stringByAppendingPathComponent:[fileName stringByDeletingPathExtension]]);
                });
            }
            
        }];
    });
}

+ (void)unzipFile:(NSString *)urlPath completion:(void (^)(NSArray<NSString *> *, NSError *))completion {
    
    NSProgress *progress = [NSProgress progressWithTotalUnitCount:-1];
    progress.cancellable = NO;
    progress.pausable = NO;
    
    // write info
    NSString *fileName = [NSString generateAUniqueText];
    NSString *temporaryDicrectory = NSTemporaryDirectory();
    NSString *destinationPath = [temporaryDicrectory stringByAppendingPathComponent:fileName];

    [SSZipArchive unzipFileAtPath:urlPath toDestination:destinationPath progressHandler:^(NSString * _Nonnull entry, unz_file_info zipInfo, long entryNumber, long total) {
        progress.totalUnitCount = total;
        progress.completedUnitCount = entryNumber;
    } completionHandler:^(NSString * _Nonnull path, BOOL succeeded, NSError * _Nonnull error) {
        if (completion) {
            if (succeeded) {
                // loop and return in destination folder all file paths
                NSError *err;
                NSArray *names = [[NSFileManager defaultManager] contentsOfDirectoryAtPath:destinationPath error:&err];
                if (err) {
                    completion(nil, err);
                } else {
                    NSMutableArray *paths = [NSMutableArray array];
                    for (NSString *name in names) {
                        [paths addObject:[destinationPath stringByAppendingPathComponent:name]];
                    }
                    completion(paths, nil);
                }
            } else {
                completion(nil, error);
            }
        }
        progress.completedUnitCount = progress.totalUnitCount;
    }];
}


@end
