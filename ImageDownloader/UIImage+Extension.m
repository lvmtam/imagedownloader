//
//  UIImage+Resizable.m
//  ImageDownloader
//
//  Created by lvmtam on 11/25/16.
//  Copyright © 2016 lvmtam. All rights reserved.
//

#import "UIImage+Extension.h"


@interface ImageCache : NSCache
- (UIImage *)cachedImageForKey:(NSString *)key;
- (void)cacheImage:(UIImage *)image forKey:(NSString *)key;
@end


@implementation ImageCache

- (UIImage *)cachedImageForKey:(NSString *)key {
    return [self objectForKey:key];
}

- (void)cacheImage:(UIImage *)image forKey:(NSString *)key {
    if (image && key) {
        [self setObject:image forKey:key];
    }
}

@end



#pragma mark -

@implementation UIImage (Extension)


+ (ImageCache *)im_imageCache {
    static ImageCache *imageCacheInstance;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        imageCacheInstance = [[ImageCache alloc] init];
    });
    return imageCacheInstance;
}



/// Scale

+ (UIImage *)imageWithFilePath:(NSString *)path cache:(BOOL)cache {
    return [UIImage imageWithFilePath:path size:CGSizeZero cache:cache];
}


+ (UIImage *)imageWithFilePath:(NSString *)path size:(CGSize)size cache:(BOOL)cache {
    
    UIImage *image = [[self im_imageCache] cachedImageForKey:path];
    if (cache && image) {
        return image;
    }
    
    // fetch image
    image = [UIImage imageWithData:[NSData dataWithContentsOfFile:path]];
    
    // resize if need
    if (!CGSizeEqualToSize(size, CGSizeZero)) {
        UIGraphicsBeginImageContext(size);
        [image drawInRect:CGRectMake(0, 0, size.width, size.height)];
        image = UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsEndImageContext();
    }
    
    if (cache && image) {
        [[self im_imageCache] cacheImage:image forKey:path];
    }
    
    return image;
}


/// PDF
+ (UIImage *)imageWithPDF:(NSString *)path size:(CGSize)size cache:(BOOL)cache {
    
    if (!path) {
        return nil;
    }
    
    NSString *key = [NSString stringWithFormat:@"%@-%@", path, NSStringFromCGSize(size)];
    UIImage *image = [[self im_imageCache] cachedImageForKey:key];
    if (cache && image) {
        return image;
    }
    
    CGPDFDocumentRef pdfDocument = [self getPDFDocumentFromPath:path];

    UIGraphicsBeginImageContext(size);
    CGContextRef context = UIGraphicsGetCurrentContext();
    // PDF page drawing expects a Lower-Left coordinate system, so we flip the coordinate system
    // before we start drawing.
    CGContextTranslateCTM(context, 0.0, size.height);
    CGContextScaleCTM(context, 1.0, -1.0);
    
    // Grab the first PDF page
    CGPDFPageRef page = CGPDFDocumentGetPage(pdfDocument, 1);
    // We're about to modify the context CTM to draw the PDF page where we want it, so save the graphics state in case we want to do more drawing
    CGContextSaveGState(context);
    // CGPDFPageGetDrawingTransform provides an easy way to get the transform for a PDF page. It will scale down to fit, including any
    // base rotations necessary to display the PDF page correctly.
    CGAffineTransform pdfTransform = CGPDFPageGetDrawingTransform(page, kCGPDFCropBox, CGRectMake(0, 0, size.width, size.height), 0, true);
    // And apply the transform.
    CGContextConcatCTM(context, pdfTransform);
    // Finally, we draw the page and restore the graphics state for further manipulations!
    CGContextDrawPDFPage(context, page);
    
    CGImageRef imageRef = CGBitmapContextCreateImage(context);
    image = [UIImage imageWithCGImage:imageRef];
    
    CGContextRestoreGState(context);
    UIGraphicsEndImageContext();
    
    if (cache && image) {
        [[self im_imageCache] cacheImage:image forKey:key];
    }

    return image;
}

+ (CGPDFDocumentRef)getPDFDocumentFromPath:(NSString *)path {
    CGPDFDocumentRef document;
    CFURLRef pdfURL = (__bridge CFURLRef)[NSURL fileURLWithPath:path];
    document = CGPDFDocumentCreateWithURL((CFURLRef)pdfURL);
    CFRelease(pdfURL);
    return document;
}


+ (void)clearCache {
    [[self im_imageCache] removeAllObjects];
}

+ (void)clearCachedImageForKey:(NSString *)key {
    if (key) {
        [[self im_imageCache] removeObjectForKey:key];
    }
}


@end
