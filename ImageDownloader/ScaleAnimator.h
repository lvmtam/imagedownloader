//
//  ScaleAnimator.h
//  ImageDownloader
//
//  Created by Tam Le on 12/7/16.
//  Copyright © 2016 lvmtam. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface ScaleAnimator : NSObject <UIViewControllerAnimatedTransitioning>
@property (nonatomic) NSTimeInterval duration;  // Default is 0.24 (seconds)
@property (nonatomic) CGRect originFrame;       // Default is CGRectZero
@property (nonatomic) BOOL presenting;
@end
