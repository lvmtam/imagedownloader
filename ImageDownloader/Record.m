//
//  Record.m
//  ImageDownloader
//
//  Created by lvmtam on 11/24/16.
//  Copyright © 2016 lvmtam. All rights reserved.
//

#import "Record.h"
#import "PendingOperations.h"
#import "RecordDownloader.h"
#import "UIImage+Extension.h"



@interface Record()
@property (nonatomic, readwrite) RecordType type;
@end



@implementation Record

- (void)setUrl:(NSURL *)url {
    _url = url;
    
    // update file extension
    NSString *fileExtension = [self getFileExtension];
    if ([fileExtension isEqualToString:@"pdf"]) {
        self.type = RecordTypePDF;
    } else if ([fileExtension isEqualToString:@"zip"]) {
        self.type = RecordTypeZip;
    } else {
        self.type = RecordTypeImage;
    }
}

- (NSString *)getFileExtension {
    if (!self.url) {
        return nil;
    }
    NSString *filename = self.url.lastPathComponent;
    NSString *fileExtension = [filename.pathExtension lowercaseString];
    return fileExtension;
}

- (UIImage *)getImage {
    switch (self.type) {
        case RecordTypeImage: /*fall-through*/
        case RecordTypeZip:
            return [UIImage imageWithFilePath:self.storedDataUrl.absoluteString cache:YES];
        case RecordTypePDF:
            return [UIImage imageWithPDF:self.storedDataUrl.absoluteString size:[UIScreen mainScreen].bounds.size cache:YES];
    }
}

- (UIImage *)getThumbnailImageWithSize:(CGSize)size {
    switch (self.type) {
        case RecordTypeImage: /*fall-through*/
        case RecordTypeZip: {
            return [UIImage imageWithFilePath:self.storedDataUrl.absoluteString size:size cache:NO];
        }
        case RecordTypePDF:
            return [UIImage imageWithPDF:self.storedDataUrl.absoluteString size:size cache:NO];
    }
}



#pragma mark - NSOperation

- (NSProgress *)start {
    
    // stop if not New
    if (self.state != RecordStateNew) {
        return nil;
    }
    
    // create and start new once
    PendingOperations *pendingOperations = [PendingOperations sharedInstance];
    RecordDownloader *operation = [[RecordDownloader alloc] initWithRecord:self];
    [pendingOperations.downloadQueue addOperation:operation];
    
    // update state
    self.state = RecordStateQueueing;
    
    // store progress
    self.progress = operation.progress;
    
    return self.progress;
}

- (void)reset {
    self.state = RecordStateNew;
    self.storedDataUrl = nil;
    self.progress = nil;
}

@end
