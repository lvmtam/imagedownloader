//
//  PhotoViewController.h
//  ImageDownloader
//
//  Created by lvmtam on 11/25/16.
//  Copyright © 2016 lvmtam. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Album.h"
#import "Record.h"


@interface PhotoViewController : UIViewController
@property (nonatomic, strong) Album *album;
@property (nonatomic) NSUInteger currentOffset;
@end
