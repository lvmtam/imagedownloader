//
//  Collection.m
//  ImageDownloader
//
//  Created by lvmtam on 11/24/16.
//  Copyright © 2016 lvmtam. All rights reserved.
//

#import "Album.h"
#import "RecordDownloader.h"
#import "PendingOperations.h"


@interface Album ()
@property (nonatomic, strong) PendingOperations *pendingOperations;
@end


@implementation Album

- (instancetype)init {
    self = [super init];
    if (self) {
        self.pendingOperations = [PendingOperations sharedInstance];
        self.progress = [NSProgress progressWithTotalUnitCount:-1];
        self.progress.cancellable = NO;
        self.progress.pausable = NO;
    }
    return self;
}


- (NSProgress *)startDownloadResources {
    // progress setup
    self.progress.totalUnitCount = self.records.count;
    
    // create and start operation for each once
    for (Record *record in self.records) {
        NSProgress *progress = [record start];
        if (progress) {
            [self.progress addChild:progress withPendingUnitCount:1];
        }
    }
    
    return self.progress;
}

- (void)reset {
    [self.progress cancel];
    self.progress = [NSProgress progressWithTotalUnitCount:-1];
    for (Record *record in self.records) {
        [record reset];
    }
}

- (void)pause {
    [self.progress pause];
}

- (void)resume {
    [self.progress resume];
}


@end
