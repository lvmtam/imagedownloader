//
//  PhotoViewController.m
//  ImageDownloader
//
//  Created by lvmtam on 11/25/16.
//  Copyright © 2016 lvmtam. All rights reserved.
//

#import "PhotoViewController.h"
#import "PhotoCell.h"
#import "UIImage+Extension.h"



@interface PhotoViewController () <UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout, UIScrollViewDelegate>
@property (weak, nonatomic) IBOutlet UILabel *pageLabel;
@property (weak, nonatomic) IBOutlet UIButton *cancelButton;
@property (weak, nonatomic) IBOutlet UICollectionView *photosView;
@end



@implementation PhotoViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // show photo at specify offset
    [self.photosView scrollToItemAtIndexPath:[NSIndexPath indexPathForRow:self.currentOffset inSection:0]
                            atScrollPosition:UICollectionViewScrollPositionCenteredHorizontally
                                    animated:NO];
    
    [self updateGUI];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:YES animated:animated];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    [self.navigationController setNavigationBarHidden:NO animated:animated];
}


- (void)updateShowingPhotoIndex {
    CGPoint initPinchPosition = CGPointMake(self.photosView.center.x + self.photosView.contentOffset.x, self.photosView.center.y + self.photosView.contentOffset.y);
    NSIndexPath *path = [self.photosView indexPathForItemAtPoint:initPinchPosition];
    self.currentOffset = path.row;
}

- (void)updateGUI {
    self.pageLabel.text = [NSString stringWithFormat:@"%tu/%tu", self.currentOffset + 1, self.album.records.count];
}


#pragma mark - Gestures


#pragma mark - UI Events

- (IBAction)cancelButtonDidPressed:(UIButton *)sender {
    // clear cached images
    [UIImage clearCache];
    // dismiss
    [self dismissViewControllerAnimated:YES completion:nil];
}



#pragma mark - UICollectionView DataSource

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return self.album.records.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    PhotoCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"PhotoCell" forIndexPath:indexPath];
    Record *record = self.album.records[indexPath.row];
    cell.imageView.image = [record getImage];
    return cell;
}



#pragma mark - UICollectionView Delegate

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section {
    return UIEdgeInsetsZero;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    return collectionView.bounds.size;
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section {
    return 0;
}

- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate {
    if (!decelerate) {
        [self updateShowingPhotoIndex];
    }
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView {
    [self updateShowingPhotoIndex];
}

- (void)setCurrentOffset:(NSUInteger)currentOffset {
    _currentOffset = currentOffset;
    [self updateGUI];
}

@end








