//
//  RecordsViewController.h
//  ImageDownloader
//
//  Created by lvmtam on 11/24/16.
//  Copyright © 2016 lvmtam. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Album.h"



@interface RecordsViewController : UIViewController <UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout>

@property (weak, nonatomic) IBOutlet UICollectionView *recordsCollectionView;

@property (nonatomic, strong) Album *album;

@end
