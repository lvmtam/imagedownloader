//
//  PhotoCell.h
//  ImageDownloader
//
//  Created by Tam Le on 12/1/16.
//  Copyright © 2016 lvmtam. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PhotoCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UIImageView *imageView;
@end
