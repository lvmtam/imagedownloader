//
//  RecordDownloader.h
//  ImageDownloader
//
//  Created by Tam Le on 11/25/16.
//  Copyright © 2016 lvmtam. All rights reserved.
//

#import "AsynchronizeOperation.h"
#import "Record.h"



@interface RecordDownloader : AsynchronizeOperation

@property (nonatomic, strong) Record *record;
@property (atomic, strong) NSProgress *progress;

- (instancetype)initWithRecord:(Record *)record;

@end
