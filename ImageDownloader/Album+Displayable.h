//
//  Album+Displayable.h
//  ImageDownloader
//
//  Created by lvmtam on 11/24/16.
//  Copyright © 2016 lvmtam. All rights reserved.
//

#import "Album.h"



typedef NS_ENUM(NSInteger, AlbumProcessState) {
    AlbumProcessStateQueuing,
    AlbumProcessStateDownloading,
    AlbumProcessStateFinished
};


@interface Album (Displayable)
@property (nonatomic, readonly) NSString *progressText;
@property (nonatomic, readonly) AlbumProcessState progressState;
@end
