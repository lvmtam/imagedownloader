//
//  PendingOperations.m
//  ImageDownloader
//
//  Created by lvmtam on 11/24/16.
//  Copyright © 2016 lvmtam. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface AsynchronizeOperation : NSOperation

// Call this method when a operation completion. It will be pop from Queue stack
- (void)finish;

@end
