//
//  RecordsViewController.m
//  ImageDownloader
//
//  Created by lvmtam on 11/24/16.
//  Copyright © 2016 lvmtam. All rights reserved.
//

#import "RecordsViewController.h"
#import "RecordCell.h"
#import "PhotoViewController.h"
#import "PendingOperations.h"
#import "ScaleAnimator.h"



static CGFloat ItemsPerRow = 4;



@interface RecordsViewController () <UIViewControllerTransitioningDelegate>
@property (nonatomic, strong) ScaleAnimator *transition;
@property (nonatomic) NSIndexPath *selectedRecordIndex;
@end



@implementation RecordsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.transition = [ScaleAnimator new];
    // self.transition.duration = 1;

    [self.recordsCollectionView reloadData];
}



#pragma mark - UICollectionViewDataSource

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return self.album.records.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    RecordCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"RecordCell" forIndexPath:indexPath];
    Record *record = self.album.records[indexPath.row];
    cell.record = record;
    return cell;
}



#pragma mark - UICollectionViewDelegateFlowLayout

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    CGFloat padding = [RecordsViewController sectionInsets].left * (ItemsPerRow + 1);
    CGFloat width = self.view.frame.size.width - padding;
    CGFloat widthPerItem = width / ItemsPerRow;
    return CGSizeMake(widthPerItem, widthPerItem);
}

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section {
    return [RecordsViewController sectionInsets];
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section {
    return [RecordsViewController sectionInsets].left;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    [collectionView deselectItemAtIndexPath:indexPath animated:NO];
    
    self.selectedRecordIndex = indexPath;
    
    PhotoViewController *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"PhotoViewController"];
    controller.transitioningDelegate = self;
    controller.album = self.album;
    controller.currentOffset = indexPath.row;
    
    [self presentViewController:controller animated:YES completion:nil];
}



#pragma mark - Transition Animation

- (id<UIViewControllerAnimatedTransitioning>)animationControllerForPresentedController:(UIViewController *)presented presentingController:(UIViewController *)presenting sourceController:(UIViewController *)source {
    
    CGRect initFrame = CGRectZero;
    if (self.selectedRecordIndex) {
        UICollectionViewCell *cell = [self.recordsCollectionView cellForItemAtIndexPath:self.selectedRecordIndex];
        initFrame = [self.recordsCollectionView convertRect:cell.frame toView:nil];
    }
    
    self.transition.originFrame = initFrame;
    self.transition.presenting = YES;
    return self.transition;
}

- (id<UIViewControllerAnimatedTransitioning>)animationControllerForDismissedController:(UIViewController *)dismissed {
    self.transition.presenting = NO;
    return self.transition;
}



#pragma mark - UI Events

- (IBAction)reloadButtonDidPressed:(id)sender {
    [[PendingOperations sharedInstance] resetOperationsBelongAlbum:self.album];
    [self.album reset];
    [self.album startDownloadResources];
    [self.recordsCollectionView reloadData];
}



#pragma mark - Private methods

+ (UIEdgeInsets)sectionInsets {
    static UIEdgeInsets insets;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
         insets = UIEdgeInsetsMake(2, 2, 2, 2);
    });
    return insets;
}

@end
