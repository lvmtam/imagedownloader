//
//  RecordCell.m
//  ImageDownloader
//
//  Created by lvmtam on 11/24/16.
//  Copyright © 2016 lvmtam. All rights reserved.
//

#import "RecordCell.h"
#import "UIImage+Extension.h"


static NSString *fractionCompletedKeyPath = @"progress.fractionCompleted";
static NSString *stateKeyPath = @"state";
static NSString *imageKeyPath = @"image";
static void *RecordCellObserverContext = &RecordCellObserverContext;


@interface RecordCell ()
@property (weak, nonatomic) IBOutlet UIImageView *imageView;
@property (weak, nonatomic) IBOutlet UILabel *title;
@property (strong, nonatomic) UIImage *image;
@end


@implementation RecordCell

- (void)awakeFromNib {
    [super awakeFromNib];
    [self setup];
}

- (instancetype)initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder:aDecoder];
    if (self) {
        [self setup];
    }
    return self;
}

- (void)setup {
}

- (void)setRecord:(Record *)record {
    if (_record) {
        [_record removeObserver:self forKeyPath:fractionCompletedKeyPath context:RecordCellObserverContext];
        [_record removeObserver:self forKeyPath:stateKeyPath context:RecordCellObserverContext];
    }
    _record = record;
    if (_record) {
        [_record addObserver:self forKeyPath:fractionCompletedKeyPath options:0 context:RecordCellObserverContext];
        [_record addObserver:self forKeyPath:stateKeyPath options:0 context:RecordCellObserverContext];
    }
    [self clearCachedImage];
    [self updateGUI];
}

- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary<NSKeyValueChangeKey,id> *)change context:(void *)context {
    if (context == RecordCellObserverContext) {
        if ([keyPath isEqualToString:fractionCompletedKeyPath] || [keyPath isEqualToString:stateKeyPath]) {
            [[NSOperationQueue mainQueue] addOperationWithBlock:^{
                [self updateGUI];
            }];
        }
    }
}

- (void)updateGUI {
    NSString *description = nil;
    switch (self.record.state) {
        case RecordStateNew:
        case RecordStateQueueing:
            description = @"Queuing...";
            break;
        case RecordStateDownloading:
            description = [NSString stringWithFormat:@"Downloading %.f%%", self.record.progress.fractionCompleted * 100];
            break;
        case RecordStateUnzipping:
            description = @"Unzipping...";
            break;
        case RecordStateFinished:
            description = nil;
            if (!self.image) {
                [self fetchImageWithURL:self.record.storedDataUrl];
            }
            break;
        case RecordStateError:
            description = @"Error";
            break;
    }
    self.title.text = description;
}

- (void)fetchImageWithURL:(NSURL *)url {
    dispatch_queue_t queue = dispatch_get_global_queue(0, 0);
    dispatch_async(queue, ^{
        CGFloat scale = [[UIScreen mainScreen] scale];
        CGSize thumnailSize = CGSizeMake(self.imageView.bounds.size.width * scale, self.imageView.bounds.size.height * scale);
        UIImage *image = [self.record getThumbnailImageWithSize:thumnailSize];
        dispatch_async(dispatch_get_main_queue(), ^{
            self.image = image;
        });
    });
}

- (void)setImage:(UIImage *)image {
    _image = image;
    [UIView transitionWithView:self.imageView duration:0.25 options:UIViewAnimationOptionTransitionCrossDissolve animations:^{
        self.imageView.image = image;
    } completion:nil];
}

- (void)clearCachedImage {
    self.image = nil;
}

- (void)prepareForReuse {
    [super prepareForReuse];
    [self clearCachedImage];
}

- (void)dealloc {
    if (self.record) {
        [self.record removeObserver:self forKeyPath:fractionCompletedKeyPath context:RecordCellObserverContext];
        [self.record removeObserver:self forKeyPath:stateKeyPath context:RecordCellObserverContext];
    }
}

@end
