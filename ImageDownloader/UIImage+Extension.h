//
//  UIImage+Resizable.h
//  ImageDownloader
//
//  Created by lvmtam on 11/25/16.
//  Copyright © 2016 lvmtam. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImage (Extension)

+ (UIImage *)imageWithPDF:(NSString *)path size:(CGSize)size cache:(BOOL)cache;
+ (UIImage *)imageWithFilePath:(NSString *)path cache:(BOOL)cache;
+ (UIImage *)imageWithFilePath:(NSString *)path size:(CGSize)size cache:(BOOL)cache;


+ (void)clearCache;
+ (void)clearCachedImageForKey:(NSString *)key;

@end
