//
//  AlbumManager.m
//  ImageDownloader
//
//  Created by lvmtam on 11/24/16.
//  Copyright © 2016 lvmtam. All rights reserved.
//

#import "AlbumManager.h"



@implementation AlbumManager



- (instancetype)init {
    self = [super init];
    if (self) {
        self.downloadQueue = [[NSOperationQueue alloc] init];
        self.downloadQueue.maxConcurrentOperationCount = 1;
        self.downloadQueue.name = @"Album download Queue";
        self.downloadsInProgress = [NSMutableDictionary dictionary];
    }
    return self;
}

+ (instancetype)sharedInstance {
    static AlbumManager *sharedInstance = nil;
    @synchronized(self) {
        if (sharedInstance == nil) {
            sharedInstance = [[self alloc] init];
        }
    }
    return sharedInstance;
}


+ (void)fetchAlbumsWithCompletion:(void (^)(NSArray<Album *> *albums, NSError *error))completion {
    
    // download and unzip album files
    [ZipFileManager downloadAndUnzip:@"https://dl.dropboxusercontent.com/u/4529715/JSON%20files%20updated.zip" completion:^(BOOL succeeded, NSString *unzipPath) {
        
        if (!succeeded) {
            if (completion) {
                completion(nil, [NSError errorWithDomain:@"imagedownloader.AlbumManager" code:1 userInfo:@{@"reason": @"fatal: unzip process has error"}]);
            }
            return;
        }
        
        // loop all files
        NSError *error;
        NSArray *names = [[NSFileManager defaultManager] contentsOfDirectoryAtPath:unzipPath error:&error];
        if (error) {
            if (completion) {
                completion(nil, [NSError errorWithDomain:@"imagedownloader.AlbumManager" code:2 userInfo:@{@"reason": @"fatal: error when reading unzip folder"}]);
            }
            return;
        }
        
        // ... and init Albums
        NSMutableArray *albums = [NSMutableArray array];
        for (NSString *name in names) {
            NSString *fileExtension = [name.pathExtension lowercaseString];
            if ([fileExtension isEqualToString:@"json"]) {
                NSString *filePath = [unzipPath stringByAppendingPathComponent:name];
                NSDictionary *json = [NSJSONSerialization JSONObjectWithData:[NSData dataWithContentsOfURL:[NSURL fileURLWithPath:filePath]] options:0 error:&error];
                if (!error) {
                    Album *album = [AlbumManager createAlbum:name json:json];
                    [albums addObject:album];
                }
            }
        }
        
        // callback
        if (completion) {
            completion(albums, nil);
        }

    }];
}


+ (Album *)createAlbum:(NSString *)name json:(NSDictionary *)json {
    
    NSMutableArray *records = [NSMutableArray array];
    for (NSString *url in json) {
        Record *record = [Record new];
        record.url = [NSURL URLWithString:url];
        [records addObject:record];
    }
    
    Album *album = [Album new];
    album.name = name;
    album.records = records;
    
    return album;
}

@end
