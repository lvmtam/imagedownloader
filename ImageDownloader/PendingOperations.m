//
//  PendingOperations.m
//  ImageDownloader
//
//  Created by lvmtam on 11/24/16.
//  Copyright © 2016 lvmtam. All rights reserved.
//

#import "PendingOperations.h"
#import "RecordDownloader.h"


@implementation PendingOperations

- (instancetype)init {
    self = [super init];
    if (self) {
        self.maxConcurrentCount = 1;
        self.downloadQueue = [[NSOperationQueue alloc] init];
        self.downloadQueue.maxConcurrentOperationCount = self.maxConcurrentCount;
        self.downloadQueue.name = @"Album download Queue";
    }
    return self;
}

+ (instancetype)sharedInstance {
    static PendingOperations *sharedInstance = nil;
    @synchronized(self) {
        if (sharedInstance == nil) {
            sharedInstance = [[self alloc] init];
        }
    }
    return sharedInstance;
}

- (void)setMaxConcurrentCount:(NSUInteger)maxConcurrentCount {
    _maxConcurrentCount = maxConcurrentCount;
    
    self.downloadQueue.maxConcurrentOperationCount = maxConcurrentCount;
}

- (void)reset {
    // cancel all operations
    [self.downloadQueue cancelAllOperations];
}

- (void)resetOperationsBelongAlbum:(Album *)album {
    for (Record *record in album.records) {
        for (RecordDownloader *operation in self.downloadQueue.operations) {
            if (record == operation.record) {
                [operation cancel];
            }
        }
    }
}

- (void)pause {
    // suspend all operations
    self.downloadQueue.suspended = YES;
}

- (void)resume {
    self.downloadQueue.suspended = NO;
}

@end
