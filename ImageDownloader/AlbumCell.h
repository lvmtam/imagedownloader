//
//  AlbumCell.h
//  ImageDownloader
//
//  Created by lvmtam on 11/24/16.
//  Copyright © 2016 lvmtam. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Album.h"



@interface AlbumCell : UITableViewCell

@property (nonatomic, strong) Album *album;

@end
