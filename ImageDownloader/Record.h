//
//  Record.h
//  ImageDownloader
//
//  Created by lvmtam on 11/24/16.
//  Copyright © 2016 lvmtam. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>



typedef NS_ENUM(NSInteger, RecordState) {
    RecordStateNew,
    RecordStateQueueing,
    RecordStateDownloading,
    RecordStateUnzipping,
    RecordStateError,
    RecordStateFinished
};


typedef NS_ENUM(NSInteger, RecordType) {
    RecordTypeImage,
    RecordTypePDF,
    RecordTypeZip
};



@interface Record : NSObject

@property (nonatomic, strong) NSURL *url;
@property (nonatomic, strong) NSURL *storedDataUrl;
@property (atomic, strong) NSProgress *progress;
@property (nonatomic) RecordState state;

@property (nonatomic, readonly) RecordType type;

- (NSProgress *)start;
- (void)reset;

// These method will read image from local device because of memory issue.
- (UIImage *)getImage;
- (UIImage *)getThumbnailImageWithSize:(CGSize)size;

@end
