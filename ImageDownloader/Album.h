//
//  Collection.h
//  ImageDownloader
//
//  Created by lvmtam on 11/24/16.
//  Copyright © 2016 lvmtam. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Record.h"


@interface Album : NSObject

@property (nonatomic, strong) NSString *name;
@property (nonatomic, strong) NSArray<Record *> *records;
@property (atomic, strong) NSProgress *progress;

- (NSProgress *)startDownloadResources;

- (void)reset;
- (void)pause;
- (void)resume;

@end
