//
//  ScaleAnimator.m
//  ImageDownloader
//
//  Created by Tam Le on 12/7/16.
//  Copyright © 2016 lvmtam. All rights reserved.
//

#import "ScaleAnimator.h"



@interface ScaleAnimator ()
@end



@implementation ScaleAnimator

- (instancetype)init {
    self = [super init];
    if (self) {
        self.duration = 0.24f;
        self.originFrame = CGRectZero;
    }
    return self;
}

- (NSTimeInterval)transitionDuration:(id<UIViewControllerContextTransitioning>)transitionContext {
    return self.duration;
}

- (void)animateTransition:(id<UIViewControllerContextTransitioning>)transitionContext {
    
    UIView *containerView = [transitionContext containerView];
    UIView *fromView = [transitionContext viewForKey:UITransitionContextFromViewKey];
    UIView *toView = [transitionContext viewForKey:UITransitionContextToViewKey];
    
    
    /// PRESENTING
    if (self.presenting) {
        
        CGFloat xfactor = self.originFrame.size.width  / toView.frame.size.width;
        CGFloat yfactor = self.originFrame.size.height / toView.frame.size.height;
        
        // scale down view before animating
        CGAffineTransform scaleTransform = CGAffineTransformMakeScale(xfactor, yfactor);
        toView.transform = scaleTransform;
        toView.center = CGPointMake(CGRectGetMidX(self.originFrame), CGRectGetMidY(self.originFrame));
        toView.clipsToBounds = true;

        // add view to container
        [containerView addSubview:toView];
        [containerView bringSubviewToFront:toView];
        
        // animate
        [UIView animateWithDuration:self.duration animations:^{
            toView.transform = CGAffineTransformIdentity;
            toView.center = CGPointMake(CGRectGetMidX(fromView.frame), CGRectGetMidY(fromView.frame));
        } completion:^(BOOL finished) {
            [transitionContext completeTransition:YES];
        }];
    }
    /// DISMISSING
    else {
        
        CGFloat xfactor = self.originFrame.size.width / fromView.frame.size.width;
        CGFloat yfactor = self.originFrame.size.height / fromView.frame.size.height;
        CGAffineTransform scaleTransform = CGAffineTransformMakeScale(xfactor, yfactor);

        // add view to container
        [containerView addSubview:toView];
        [containerView bringSubviewToFront:fromView];

        // animate
        [UIView animateWithDuration:self.duration animations:^{
            fromView.transform = scaleTransform;
            fromView.center = CGPointMake(CGRectGetMidX(self.originFrame), CGRectGetMidY(self.originFrame));
        } completion:^(BOOL finished) {
            [transitionContext completeTransition:YES];
        }];
    }
}

@end
