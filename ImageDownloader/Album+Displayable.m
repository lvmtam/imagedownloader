//
//  Album+Displayable.m
//  ImageDownloader
//
//  Created by lvmtam on 11/24/16.
//  Copyright © 2016 lvmtam. All rights reserved.
//

#import "Album+Displayable.h"


@implementation Album (Displayable)

- (NSString *)progressText {
    switch (self.progressState) {
        case AlbumProcessStateQueuing:
            return @"Queuing...";
        case AlbumProcessStateDownloading:
            return @"Downloading...";
        case AlbumProcessStateFinished:
            return @"Finished";
    }
}

- (AlbumProcessState)progressState {
    CGFloat completed = self.progress.completedUnitCount;
    CGFloat total = self.progress.totalUnitCount;
    // Queue state
    if (completed == 0 && self.progress.fractionCompleted == 0) {
        return AlbumProcessStateQueuing;
    }
    // Downloading state
    // discuss: an NSProgress is finished if it's not indeterminate, and the completedUnitCount > totalUnitCount.
    if ((completed >= total && completed > 0 && total > 0) || (completed > 0 && total == 0)) {
        return AlbumProcessStateFinished;
    }
    // Finished state
    return AlbumProcessStateDownloading;
}


@end
