//
//  AppDelegate.h
//  ImageDownloader
//
//  Created by lvmtam on 11/24/16.
//  Copyright © 2016 lvmtam. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;
@property (copy) void (^backgroundSessionCompletionHandler)();

- (void)setNetworkActivityIndicatorVisible:(BOOL)visible;
- (void)setLoadingIndicatorVisible:(BOOL)visible;

@end

