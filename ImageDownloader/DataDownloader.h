//
//  DataDownloader.h
//  ImageDownloader
//
//  Created by lvmtam on 11/24/16.
//  Copyright © 2016 lvmtam. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AsynchronizeOperation.h"
#import "Record.h"



@interface DataDownloader : AsynchronizeOperation

@property (nonatomic, strong) Record *record;
@property (nonatomic, strong) NSError *downloadError;
@property (atomic, strong) NSProgress *progress;

- (instancetype)initWithRecord:(Record *)record;

@end
